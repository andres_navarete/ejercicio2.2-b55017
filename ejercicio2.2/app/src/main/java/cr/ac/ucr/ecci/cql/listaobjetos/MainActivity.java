package cr.ac.ucr.ecci.cql.listaobjetos;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;
public class MainActivity extends AppCompatActivity {
    // definimos la lista de datos
    private ListView listView;
    //
    private List<Tip> mTips;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Obtenemos la lista
        listView = (ListView) findViewById(R.id.list);
        //
        mTips = new ArrayList<Tip>();
        mTips.add(new Tip("Agua", "01", "Al menos 8 vasos al día"));
        mTips.add(new Tip("Vino", "02", "No exceda una copa al día"));
        mTips.add(new Tip("Café", "03", "Evite tomarlo"));
        mTips.add(new Tip("Carnes", "04", "Al menos tres veces a la semana"));
        mTips.add(new Tip("Hamburguesa", "05", "Solo caseras y bajas en grasa"));
        // definimos el adaptador para la lista
        ArrayAdapter<Tip> adapter = new ArrayAdapter<Tip>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, mTips);
        // asignamos el adaptador al ListView
        listView.setAdapter(adapter);
        // ListView Item Click Listener para las llamadas a las opciones de los items
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // ListView Clicked item index
                int itemPosition = position;
                // ListView Clicked item value
                Tip item = (Tip)listView.getItemAtPosition(position);
                // Show Alert
                Toast.makeText(getApplicationContext(), "Position: " + itemPosition +
                        " ListItem: " + item.toString(), Toast.LENGTH_LONG).show();
            }
        });
    }
}